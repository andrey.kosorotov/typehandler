﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;

namespace TypeHandler
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CsvLoader csvLoader;
        private TypeInfo typeInfo;
        private XmlEncoder xmlEncoder;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            typeInfo = new TypeInfo("../../TypeInfos.json");
            csvLoader = new CsvLoader();
        }

        private void LoadCsv_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                Multiselect = false,
                Filter = "CSV files (*.csv)|*.csv",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer)  //
            };

            if (openFileDialog.ShowDialog() == true)
            {
                csvLoader = new CsvLoader();
                csvLoader.LoadCsv(openFileDialog.FileName);
                csvData.ItemsSource = csvLoader.csvData;
                csvPath.Text = openFileDialog.FileName;
            }
            
        }

        private void ConvertToXml_Click(object sender, RoutedEventArgs e)
        {
            if (csvLoader.csvData == null || csvLoader.csvData.Count == 0)
            {
                MessageBox.Show("Нет данных для созадания XML. Загрузите CSV");
                return;
            }

            DataCombiner dataCombiner = new DataCombiner();
            List<CsvRow> data;
            if (csvData.SelectedItems.Count == 0)
            {
                data = csvLoader.csvData;
            }
            else
            {
                data = new List<CsvRow>();
                for (int i = 0; i < csvData.SelectedItems.Count; i++)
                {
                    CsvRow dataRow = (CsvRow)csvData.SelectedItems[i];
                    data.Add(dataRow);
                }
            }
            dataCombiner.Combine(data, typeInfo.typeInfoData);
            SaveFileDialog saveFileDialog = new SaveFileDialog()
            {
                Filter = "XML files (*.xml)|*.xml",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer)  //
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                xmlEncoder = new XmlEncoder();
                xmlEncoder.XmlWriteToFile(saveFileDialog.FileName, dataCombiner.CombinedDataRows);
            }
        }
    }
}

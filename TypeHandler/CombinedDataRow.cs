﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeHandler
{
    class CombinedDataRow
    {
        public CombinedDataRow(string Tag, int Offset)
        {
            this.Tag = Tag;
            this.Offset = Offset;
        }

        public string Tag { get; set; }
        public int Offset { get; set; }
    }
}

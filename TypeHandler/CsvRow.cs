﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeHandler
{
    class CsvRow
    {
        public CsvRow(string Tag, string Type, int Address)
        {
            this.Tag = Tag;
            this.Type = Type;
            this.Address = Address;
        }

        public string Tag { get; set; }
        public string Type { get; set; }
        public int Address { get; set; }
    }
}

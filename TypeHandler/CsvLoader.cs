﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;

namespace TypeHandler
{
    class CsvLoader
    {
        public List<CsvRow> csvData;

        public CsvLoader()
        {
            csvData = new List<CsvRow>();
        }

        public void LoadCsv(string csvFileName)
        {
            using (StreamReader r = new StreamReader(csvFileName))
            {
                string csv = r.ReadToEnd();

                HandleCsvData(csv);
            }
        }

        private void HandleCsvData(string csv)
        {
            string[] csvRows = csv.Split('\n');
            string tag = "";
            string type = "";
            int address = 0;
            try
            {
                if (csvRows.Length > 0)
                {
                    for (int i = 1; i < csvRows.Length-1; i++)
                    {
                        string[] csvRowFields = csvRows[i].Split(';');
                        
                        tag = csvRowFields[0].Trim();
                        type = csvRowFields[1].Trim();
                        try
                        {
                            address = Int32.Parse(csvRowFields[2]);
                        }
                        catch (Exception ex)
                        {
                            address = 0;
                        }
                        csvData.Add(new CsvRow(tag, type, address));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Некорректный файл CSV");
            }

        }
    }
}

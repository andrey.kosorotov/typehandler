﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using System.Text.Json;
using Newtonsoft.Json;

namespace TypeHandler
{

    class TypeInfoModel
    {
        public string TypeName;
        public Dictionary<string, string> Propertys;
    }

    class TypeInfosModel
    {
        public List<TypeInfoModel> TypeInfos;
    }

    class TypeInfo
    {
        public TypeInfosModel jsonData;

        public Dictionary<string, List<KeyValuePair<string, string>>> typeInfoData;

        public TypeInfo(string jsonFile)
        {
            using (StreamReader r = new StreamReader(jsonFile))
            {
                string json = r.ReadToEnd();
                jsonData = JsonConvert.DeserializeObject<TypeInfosModel>(json);
            }

            typeInfoData = new Dictionary<string, List<KeyValuePair<string, string>>>();
            
            for (int i = 0; i < jsonData.TypeInfos.Count; i++)
            {
                typeInfoData[jsonData.TypeInfos[i].TypeName] = new List<KeyValuePair<string, string>>();
                foreach (KeyValuePair<string, string> p in jsonData.TypeInfos[i].Propertys)
                {
                    typeInfoData[jsonData.TypeInfos[i].TypeName].Add(p);
                }
            }
        }
    }
}

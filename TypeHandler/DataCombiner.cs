﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeHandler
{
    class DataCombiner
    {
        public List<CombinedDataRow> CombinedDataRows;

        public DataCombiner()
        {
            CombinedDataRows = new List<CombinedDataRow>();
        }

        public void Combine(List<CsvRow> csvData, Dictionary<string, List<KeyValuePair<string, string>>> typeInfoData)
        {
            int offset = 0;
            for (int i = 0; i < csvData.Count; i++)
            {
                string type = csvData[i].Type;
                List<KeyValuePair<string, string>> properties = typeInfoData[type];
                foreach (KeyValuePair<string, string> p in properties)
                {
                    CombinedDataRows.Add(new CombinedDataRow(csvData[i].Tag + "." + p.Key, offset));
                    offset = CalcOffset(p.Value, offset);
                }
            }
        }

        private int CalcOffset(string DataType, int InitialOffset)
        {
            int offset = InitialOffset;
            
            switch (DataType)
            {
                case "int":
                    offset += 4;
                    break;
                case "double":
                    offset += 8;
                    break;
                case "bool":
                    offset += 2;
                    break;
                default:
                    break;
            }

            return offset;
        }

        internal void Combine(List<CsvRow> csvData, Dictionary<string, List<KeyValuePair<string, string>>> typeInfoData, IList selectedItems)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace TypeHandler
{
    class XmlEncoder
    {

        public XmlEncoder()
        {
            
        }

        public void XmlWriteToFile(string XmlFilePath, List<CombinedDataRow> CombinedDataRows)
        {
            using (XmlWriter writer = XmlWriter.Create(XmlFilePath))
            {
                writer.WriteStartElement("root");

                for (int i = 0; i < CombinedDataRows.Count; i++)
                {
                    writer.WriteStartElement("item");
                    writer.WriteAttributeString("Binding", "Introduced");
                    writer.WriteElementString("node-path", CombinedDataRows[i].Tag);
                    writer.WriteElementString("address", CombinedDataRows[i].Offset.ToString());
                    writer.WriteEndElement();
                }
         
                writer.WriteEndElement();
                writer.Flush();
            }
        }
    }
}
